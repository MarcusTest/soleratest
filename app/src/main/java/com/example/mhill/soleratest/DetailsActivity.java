package com.example.mhill.soleratest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.mhill.soleratest.dao.BooksDao;
import com.example.mhill.soleratest.fragments.DetailsActivityFragment;

public class DetailsActivity extends AppCompatActivity {

    private static final String ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        int id = getIntent().getIntExtra(ID, 0);

        if(id != 0){
            BooksDao booksDao = BooksDao.getBooksDao(this);
            String url = booksDao.getPreviewUrl(id);

            Fragment fragment = new DetailsActivityFragment();
            Bundle args = new Bundle();
            args.putString(DetailsActivityFragment.URL,url);
            fragment.setArguments(args);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.place_holder_details, fragment)
                    .commit();
        }
    }

    public static void startDetailsActivity(Context context, int id){
        context.startActivity(new Intent(context, DetailsActivity.class).putExtra(ID, id));
    }

}
