package com.example.mhill.soleratest;


import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;


import com.example.mhill.soleratest.dao.BooksDao;
import com.example.mhill.soleratest.fragments.DetailsActivityFragment;
import com.example.mhill.soleratest.fragments.MainActivityFragment;
import com.example.mhill.soleratest.services.DownloadIntentService;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = MainActivity.class.getSimpleName();

    private MainActivityFragment mainFragment;
    private static final String NOTIFY_DATA_CHANGED = "com.example.mhill.soleratest.NOTIFY_DATA_CHANGED";

    FrameLayout frameLayout;

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(mainFragment!=null) {

                mainFragment.refreshCursor();
                ViewGroup.LayoutParams params = frameLayout.getLayoutParams();
                params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }

        }
    };
    private Context context;
    private boolean twoPane;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());


        ButterKnife.bind(this);

        mainFragment = new MainActivityFragment();
        frameLayout = (FrameLayout) findViewById(R.id.place_holder_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.place_holder_main,mainFragment).commit();

        context = this;

        if(findViewById(R.id.container)!=null) {

            twoPane = true;
            Log.i(TAG,"two pane");
            mainFragment.setOnItemClickListener(new MainActivityFragment.OnItemClickListener() {
                @Override
                public void onItemClick(int id) {

                    BooksDao booksDao = BooksDao.getBooksDao(context);
                    String url = booksDao.getPreviewUrl(id);


                    DetailsActivityFragment detailsFragment = DetailsActivityFragment.initailiseFragment(url);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.place_holder_details, detailsFragment).commit();
                }
            });
        }
        else{
            twoPane = false;
            Log.i(TAG,"single pane");
            mainFragment.setOnItemClickListener(new MainActivityFragment.OnItemClickListener() {
                @Override
                public void onItemClick(int id) {
                    DetailsActivity.startDetailsActivity(context, id);
                }
            });

        }




    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(NOTIFY_DATA_CHANGED));
        Log.d(TAG, "Broadcast Receiver registered");

    }

    @Override
    public void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if(item.getItemId()==R.id.action_search_btn){
            SearchActivity.startSearchActivity(this);
        }


        return super.onOptionsItemSelected(item);
    }


    public static void refeshData(Context context) {
        Intent intent = new Intent();
        intent.setAction(NOTIFY_DATA_CHANGED);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


}
