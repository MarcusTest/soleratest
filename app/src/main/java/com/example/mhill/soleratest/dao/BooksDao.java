package com.example.mhill.soleratest.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.mhill.soleratest.database.DBHelper;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volumes;

/**
 * Created by mhill on 23/03/16.
 */
public class BooksDao {

    private final Context context;
    private static BooksDao booksDao;
    private boolean isOpen;
    private SQLiteDatabase db;


    private BooksDao(Context context){
        this.context = context;
        db = new DBHelper(context).getWritableDatabase();
        this.isOpen = true;
    }


    public static BooksDao getBooksDao(Context context){
        if(booksDao != null){
            return booksDao;
        }else{
            booksDao = new BooksDao(context);
            return booksDao;
        }
    }
    public void addVolumes(Volumes volumes){


        db.delete(DBHelper.BOOK_TABLE, null, null);


        for(Volume v: volumes.getItems()){
            ContentValues values = new ContentValues();
            //values.put(DBHelper.KEY_COLUMN, v.getId());
            values.put(DBHelper.AUTHOR, v.getVolumeInfo().getAuthors().get(0));
            values.put(DBHelper.TITLE_COLUMN, v.getVolumeInfo().getTitle());
            values.put(DBHelper.COVER_COLUMN, v.getVolumeInfo().getImageLinks().getThumbnail().toString());
            values.put(DBHelper.DETAILS_COLUMN, v.getVolumeInfo().getPreviewLink());
            db.insert(DBHelper.BOOK_TABLE, null, values);
        }



    }



    public Cursor getCursor() {

        return db.query(DBHelper.BOOK_TABLE, null, null, null, null, null, null);

    }

    public void close(){
        if(isOpen){
            db.close();
        }
    }

    public String getPreviewUrl(int id) {
        Cursor c = db.query(DBHelper.BOOK_TABLE, new String[]{DBHelper.DETAILS_COLUMN}, DBHelper.KEY_COLUMN + " = " + id, null, null, null, null, null);
        if(c.getCount()>0){
            c.moveToFirst();
            String url = c.getString(0);
            c.close();
            return url;

        }

        return null;
    }
}
