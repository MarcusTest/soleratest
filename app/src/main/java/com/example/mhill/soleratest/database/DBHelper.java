package com.example.mhill.soleratest.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volumes;

/**
 * Created by Marcus Hill on 07/03/2016.
 */

public class DBHelper extends SQLiteOpenHelper {

        private static final int DB_VERSION = 1;
        private static final String DB_NAME = "shazam.db";

        public static final String BOOK_TABLE = "book";
        public static final String KEY_COLUMN = "_id";
        public static final String AUTHOR = "authour";
        public static final String TITLE_COLUMN = "title";
        public static final String COVER_COLUMN = "cover";
        public static final String DETAILS_COLUMN = "details";

        private static final String CREATE_BOOKS_TABLE = "CREATE TABLE " + BOOK_TABLE + " (" + KEY_COLUMN +
                " INTEGER PRIMARY KEY, " + AUTHOR + " TEXT NOT NULL, " + TITLE_COLUMN  + " TEXT NOT NULL, " + COVER_COLUMN +" TEXT NOT NULL," + DETAILS_COLUMN + " TEXT NOT NULL);";

        private static final String STREAM_TABLE = "streams";


        public DBHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, DB_NAME, factory, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(CREATE_BOOKS_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE " + BOOK_TABLE);

        }




    }
