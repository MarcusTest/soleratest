package com.example.mhill.soleratest.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.mhill.soleratest.DetailsActivity;
import com.example.mhill.soleratest.R;
import com.example.mhill.soleratest.dao.BooksDao;
import com.example.mhill.soleratest.database.DBHelper;
import com.example.mhill.soleratest.model.Book;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private static final String TAG = MainActivityFragment.class.getSimpleName();
    @Bind(R.id.recycle_view)
    RecyclerView recyclerView;

    private BookAdapter bookAdapter;
    private OnItemClickListener onItemClickListener;

    public MainActivityFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        BooksDao booksDao = BooksDao.getBooksDao(getContext());
        bookAdapter = new BookAdapter(null);

        //recyclerView.setBackground();
        recyclerView.setAdapter(bookAdapter);

        refreshCursor();

    }


    public void refreshCursor() {
        Cursor c = BooksDao.getBooksDao(getContext()).getCursor();
        bookAdapter.swapCursor(c);
    }



    class BookAdapter extends RecyclerView.Adapter{



        private Cursor c;

        public BookAdapter(Cursor c) {

            this.c = c;

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = View.inflate(getContext(), R.layout.list_book, null);
            BooksHolder booksHolder = new BooksHolder(itemView);
            return booksHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if(c!=null && c.getCount() != 0){
                c.moveToPosition(position);
                BooksHolder bookHolder = (BooksHolder) holder;
                Picasso.with(getContext()).load(c.getString(c.getColumnIndex(DBHelper.COVER_COLUMN))).resize(200, 200).centerInside().into(bookHolder.coverImage);
                bookHolder.authorText.setText(c.getString(c.getColumnIndex(DBHelper.AUTHOR)));
                bookHolder.titleText.setText(c.getString(c.getColumnIndex(DBHelper.TITLE_COLUMN)));
                bookHolder.id = c.getInt(c.getColumnIndex(DBHelper.KEY_COLUMN));

            }

        }

        @Override
        public int getItemCount() {
            if(c != null) {
                return c.getCount();
            }else{
                return 0;
            }
        }



        public void swapCursor(Cursor c) {
            this.c = c;
            notifyDataSetChanged();
        }

        class BooksHolder extends RecyclerView.ViewHolder{

            @Bind(R.id.cover_image)
            ImageView coverImage;
            @Bind(R.id.title_text)
            TextView titleText;
            @Bind(R.id.author_text)
            TextView authorText;
            protected int id;

            public BooksHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if(onItemClickListener != null){
                            onItemClickListener.onItemClick(id);
                        }

                    }
                });
            }



        }
    }

    public interface OnItemClickListener{
        void onItemClick(int id);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}


