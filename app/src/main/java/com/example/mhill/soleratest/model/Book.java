package com.example.mhill.soleratest.model;

/**
 * Created by mhill on 23/03/16.
 */
public class Book {

    private static String author, title, coverImage, details;

    public static String getAuthor() {
        return author;
    }

    public static void setAuthor(String author) {
        Book.author = author;
    }

    public static String getTitle() {
        return title;
    }

    public static void setTitle(String title) {
        Book.title = title;
    }

    public static String getDetails() {
        return details;
    }

    public static void setDetails(String details) {
        Book.details = details;
    }

    public static String getCoverImage() {
        return coverImage;
    }

    public static void setCoverImage(String coverImage) {
        Book.coverImage = coverImage;
    }
}
