package com.example.mhill.soleratest.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.mhill.soleratest.MainActivity;
import com.example.mhill.soleratest.R;
import com.example.mhill.soleratest.dao.BooksDao;
import com.example.mhill.soleratest.fragments.MainActivityFragment;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.BooksRequestInitializer;
import com.google.api.services.books.Books.Volumes.List;
import com.google.api.services.books.model.Volumes;

import java.io.IOException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadIntentService extends IntentService {

    public static final String ACTION_FETCH_NEW_ITEMS = "com.example.mhill.soleratest.Services.action.ACTION_FETCH_NEW_ITEMS";

    // TODO: Rename parameters
    private static final String QUERY = "com.example.mhill.soleratest.Services.extra.PARAM2";
    private static final String APPLICATION_NAME = "SeleroTest";
    private static final String TAG = DownloadIntentService.class.getSimpleName();

    public DownloadIntentService() {
        super("DownloadIntentService");
    }

    /**
     * Starts this service to perform action FetchNewItems with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFetchNewItems(Context context, String query) {
        Intent intent = new Intent(context, DownloadIntentService.class);
        intent.setAction(ACTION_FETCH_NEW_ITEMS);
        intent.putExtra(QUERY, query);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_NEW_ITEMS.equals(action)) {
                ActionFetchNewItems(getString(R.string.api_key), intent.getExtras().getString(QUERY));
            }
        }
    }

    /**
     * Handle action ActionFetchNewItems in the provided background thread with the provided
     * parameters.
     */
    private void ActionFetchNewItems(String credential, String query) {


        // Set up BooksDao client.
        Books books;
        List volumesList = null;
        try {
            JsonFactory jsonFactory = new JacksonFactory();
            books = new Books.Builder(new NetHttpTransport(), jsonFactory, null)
                    .setApplicationName(APPLICATION_NAME)
                    .setGoogleClientRequestInitializer(new BooksRequestInitializer(credential))
                    .build();
            Log.d(TAG, "Getting Volumes...");
            volumesList = books.volumes().list(query);
            volumesList.setFilter("ebooks");

            // Execute the query.
            Volumes volumes = volumesList.execute();
            Log.d(TAG,"VOLUMES = " + volumes.size());
            BooksDao booksDao = BooksDao.getBooksDao(this);
            booksDao.addVolumes(volumes);

            //Send broadcast to MainActvity

            MainActivity.refeshData(this);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
